#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <sys/xattr.h>
#include <sys/stat.h>
#include <map>
#include <vector>

std::map<std::string,std::string> help_topics;


bool file_exists(const char *file_path) {
    struct stat buf;
    return (stat(file_path, &buf) == 0);
}


bool attribute_exists(const char *file_path, const char *attr_name) {
    if (getxattr(file_path, attr_name, NULL, 0, 0, 0) >= 0) { // attr size <0 if doesn't exist
        return true;
    }
    return false;
}


int print_help(const char *command_name) {
    std::string help_text;
    
    if ((help_text = help_topics[std::string (command_name)]) != "") { //string returned from map
        std::cout << help_text << std::endl;
        return 0;
    } else {
        std::cerr << "stegosaursrc: no help topic for '" << command_name << "'" << std::endl;
        return -1;
    }
}


int print_list(const char *file_path) {
    char *attr_names; //attr_names will look like "attribute1\0arrtibute2\0..."
    char *p;
    int  names_length;
    int  attr_size;
    
    if (! file_exists(file_path)) {
        std::cerr << "stegosaursrc: file '" << file_path << "' not found" << std::endl;
        return -1;
    }
    
    names_length = listxattr(file_path, NULL, 0, 0); //find size for attr_names
    attr_names = new char[names_length + 1];
    listxattr(file_path, attr_names, names_length, 0); //populate attr_names
    attr_names[names_length] = '\0';
    p = attr_names; //second pointer to prevent orphaned characters after strchr()
    
    while (*p) {
        attr_size = getxattr(file_path, p, NULL, 0, 0, 0); //don't care about data, just want size
        std::cout << p << "\t" << attr_size << std::endl;
        p = strchr(p, '\0');
        p++;
    }
    delete [] attr_names;
    return 0;
}


int make_xattr(const char *file_path, const char *attr_name, const char *message,
               const char *password, bool replace) {
    int option = XATTR_CREATE; //error if already exists
    
    if (! file_exists(file_path)) {
        std::cerr << "stegosaursrc: file '" << file_path << "' not found" << std::endl;
        return -1;
    }
    if (replace) {
        option = 0; //create or replace
    }
    if (password && password[0]) {
        std::cout << "encrypt it!" << std::endl;
        return 0;
    } else {
        return setxattr(file_path, attr_name, message, strlen(message), 0, option);
    }
}


int print_xattr(const char *file_path, const char *attr_name, const char *password) {
    int  attr_size;
    char *attr_text;
    
    if (! file_exists(file_path)) {
        std::cerr << "stegosaursrc: file '" << file_path << "' not found" << std::endl;
        return -1;
    } else if (! attribute_exists(file_path, attr_name)) {
        std::cerr << "stegosaursrc: attribute '" << attr_name << "' not found" << std::endl;
        return -1;
    }
    
    if (password && password[0]) {
        std::cout << "decrypt it!" << std::endl;
    } else {
        attr_size = getxattr(file_path, attr_name, NULL, 0, 0, 0); //find size for buffer, ignore data
        attr_text = new char[attr_size + 1];
        getxattr(file_path, attr_name, attr_text, attr_size, 0, 0); //fill buffer
        attr_text[attr_size] = '\0';
        std::cout << attr_text << std::endl;
        delete [] attr_text;
    }
    return 0;
}


int delete_xattr(const char *file_path, const char *attr_name) {
    if (! file_exists(file_path)) {
        std::cerr << "stegosaursrc: file '" << file_path << "' not found" << std::endl;
        return -1;
    } else if (! attribute_exists(file_path, attr_name)) {
        std::cerr << "stegosaursrc: attribute '" << attr_name << "' not found" << std::endl;
        return -1;
    }
    return removexattr(file_path, attr_name, 0);
}


int archive_xattr(const char *path, const char* out_name, std::vector<std::string> files) {
    char *exec[7 + files.size()];
    const char *literals[5] = {"tar", "-czf", "steg_out.tar.gz", "-C", "."};
    char *full_path;
    
    if (files.size() == 0) {
        exec[5] = new char[2];
        strcpy(exec[5], literals[4]);
        exec[6] = NULL; //null last index for execvp
    } else {
        for (size_t i = 0; i < files.size(); i++) {
            full_path = new char[files[0].length() + strlen(path) + 1];
            strcat(full_path, path); //concatination for file checking
            strcat(full_path, files[i].c_str());
            if (! file_exists(full_path)) {
                std::cerr << "stegosaursrc: file '" << full_path << "' not found" << std::endl;
                return -1;
            }
            full_path[0] = '\0';
            delete [] full_path;
        }
        size_t i;
        for (i = 0; i < files.size(); i++) {
            exec[5+i] = new char[files[i].length()];
            strcpy(exec[5+i], files[i].c_str());
        }
        exec[5+i] = NULL; //null last index for execvp
    }
    
    for (int i = 0; i < 4; i++) {
        if (i != 2) {
            exec[i] = new char [strlen(literals[i])+1]; //new char -> strcpy b/c execvp needs non const
            strcpy(exec[i], literals[i]);
        }
    }
    
    if (out_name[0] != '\0') {
        exec[2] = new char[strlen(out_name)+1];
        strcpy(exec[2], out_name);
    } else {
        exec[2] = new char[16];
        strcpy(exec[2], literals[2]); //default archive name
    }
    
    exec[4] = new char[strlen(path)+1];
    strcpy(exec[4], path);
    
    if (! fork()) { //exec in child
        execvp(exec[0], exec);
    }
    
    for (uint i = 0; i < 6 + files.size(); i++) {
        delete [] exec[i];
    }
    return 0;
}

int unarchive_xattr(const char *file_path) {
    char *exec[4];
    const char *literals[2] = {"tar", "-xzf"};
    
    if (! file_exists(file_path)) {
        std::cerr << "stegosaursrc: file '" << file_path << "' not found" << std::endl;
        return -1;
    }
    
    for (int i = 0; i < 2; i++) {
        exec[i] = new char[strlen(literals[i])+1]; //new char -> strcpy b/c execvp needs non const
        strcpy(exec[i], literals[i]);
    }
    
    exec[2] = new char[strlen(file_path)+1];
    strcpy(exec[2], file_path);
    exec[3] = NULL;
    
    for (int i = 0; i < 3; i++) {
        std::cout << exec[i] << std::endl;
    }
    
    if (! fork()) { //exec in child
        execvp(exec[0], exec);
    }
    
    return 0;
    
}


int main (int argc, char **argv) {
    
    help_topics["help"] =
        "Show help information for stegosaursrc and its subcommands\n"
        "For help with specific commands run:\n"
        "    stegosaursrc help <command>\n"
        "Commands:\n"
        "    help:      this help page or help pages for specific commands\n"
        "    write:     make a new extended attribute\n"
        "    read:      prints the extended attribute message\n"
        "    delete:    deletes an extended attribute\n"
        "    list:      prints name and length of all extended attributes of a file\n"
        "    archive:   turns specified directory/files to tar.gz archive\n"
        "    unarchive: extracts tar.gz file preserving extended attributes";
    help_topics["list"] =
        "Usage: stegosaursrc list <file-path>\n"
        "    prints name and length of all extended attributes of a file\n"
        "Arguments:\n"
        "    <file-path>: path to the file with attributes to list";
    help_topics["write"] =
        "Usage: stegosaursrc write [-m message] [-p password] [-f] <file-path> <attribute>\n"
        "    make a new extended attribute\n"
        "Arguments:\n"
        "    <file-path>: path to the file which contains the attribute\n"
        "    <attribute>: name of the attribute to write to\n"
        "Options:\n"
        "    -m <message>: text to write to the attribute\n"
        "    -p <password>: password used to encrypt the message\n"
        "    -f: force overwrite of existing attribute";
    help_topics["delete"] =
        "Usage: stegosaursrc delete <file-path> <attribute>\n"
        "    delete an extended attribute\n"
        "Arguments:\n"
        "    <file-path>: path to the file which contains the attribute\n"
        "    <attribute>: name of the attribute to delete";
    help_topics["read"] =
        "Usage: stegosaursrc read [-p password] <file-path> <attribute>\n"
        "    prints the extended attribute message\n"
        "Arguments:\n"
        "    <file-path>: path to the file which contains the attribute\n"
        "    <attribute>: name of the attribute containing the message to read\n"
        "Options:\n"
        "    -p <password>: password used to decrypt the message";
    help_topics["archive"] =
        "Usage: stegosaursrc archive [-f file-name] [-n archive-name] <directory-path>\n"
        "   turns specified directory/files to tar.gz archive\n"
        "   preserves extended attributes of files\n"
        "Arguments:\n"
        "   <directory-path>: path to the directory to archive\n"
        "Options:\n"
        "   -f <file-name>: name of the file in the directory specified by directory path\n"
        "       Use this option to specify that specific files, rather than the entire\n"
        "       directory should be archived.\n"
        "       Use this option before each file: -f file1 -f file 2\n"
        "   -n <archive-name>: name of the output tar.gz file (default steg_out.tar.gz)";
    help_topics["unarchive"] =
        "Usage: stegosaursrc unarchive <archive-path>\n"
        "   unarchives tar.gz file preserving extended attributes\n"
        "Arguments:\n"
        "   <archive-path>: path to the tar.gz file to extract";
    
    if (argc > 1) {
        std::string command_name = argv[1];
        std::string option = "";
        std::string file_path = "";
        std::string attr_name = "";
        std::string message = "";
        std::string password = "";
        int requiredArgs;
        
        if (command_name == "help" || command_name == "-h" || command_name == "--help") {
            if (argv[2]) {
                return print_help(argv[2]);
            } else {
                const char *temp = "help";
                return print_help(temp);
            }
            
        } else if (command_name == "list") {
            if (argc == 3) {
                file_path = argv[2];
                return print_list(file_path.c_str());
            } else {
                print_help(argv[1]);
                return -1;
            }
            
        } else if (command_name == "write") {
            bool force = false;
            requiredArgs = 2;
            for (int i = 2; i < argc; i++) {
                option = argv[i];
                if (option == "-m") {
                    if (argc > ++i) {
                        message = argv[i];
                    } else {
                        std::cerr << "stegosaursrc: '-m' requires a parameter" << std::endl;
                        return -1;
                    }
                } else if (option == "-p") {
                    if (argc > ++i) {
                        password = argv[i];
                    } else {
                        std::cerr << "stegosaursrc: '-p' requires a parameter" << std::endl;
                        return -1;
                    }
                } else if (option == "-f") {
                    force = true;
                } else if (file_path == "") {
                    file_path = argv[i];
                    requiredArgs--;
                } else if (attr_name == "") {
                    attr_name = argv[i];
                    requiredArgs--;
                }
            }
            if (requiredArgs == 0) {
                return make_xattr(file_path.c_str(), attr_name.c_str(),
                                  message.c_str(), password.c_str(), force);
            } else {
                print_help(argv[1]);
                return -1;
            }
            
        } else if (command_name == "delete") {
            if (argc == 4) {
                file_path = argv[2];
                attr_name = argv[3];
                return delete_xattr(file_path.c_str(), attr_name.c_str());
            } else {
                print_help(argv[1]);
                return -1;
            }
            
        } else if (command_name == "read") {
            requiredArgs = 2;
            for (int i = 2; i < argc; i++) {
                option = argv[i];
                if (option == "-p") {
                    if (argc > ++i) {
                        password = argv[i];
                    } else {
                        std::cerr << "stegosaursrc: '-p' requires a parameter" << std::endl;
                        return -1;
                    }
                } else if (file_path == "") {
                    file_path = argv[i];
                    requiredArgs--;
                } else if (attr_name == "") {
                    attr_name = argv[i];
                    requiredArgs--;
                }
            }
            if (requiredArgs == 0) {
                return print_xattr(file_path.c_str(), attr_name.c_str(), password.c_str());
            } else {
                print_help(argv[1]);
                return -1;
            }
        } else if (command_name == "archive") {
            std::string path = "";
            std::string out_name = "";
            std::vector<std::string> files;
            requiredArgs = 1;
            
            for (int i = 2; i < argc; i++) {
                option = argv[i];
                if (option == "-n") {
                    if (argc > ++i) {
                        out_name = argv[i];
                    } else {
                        std::cerr << "stegosaursrc: '-n' requires a parameter" << std::endl;
                        return -1;
                    }
                } else if (option == "-f") {
                    if (argc > ++i) {
                        files.push_back((std::string) argv[i]);
                    } else {
                        std::cerr << "stegosaursrc: '-f' requires a parameter" << std::endl;
                        return -1;
                    }
                } else if (path == "") {
                    path = argv[i];
                    requiredArgs--;
                }
            }
            if (requiredArgs == 0) {
                return archive_xattr(path.c_str(), out_name.c_str(), files);
            } else {
                print_help(argv[1]);
                return -1;
            }
        } else if (command_name == "unarchive") {
            if (argc == 3) {
                file_path = argv[2];
                return unarchive_xattr(file_path.c_str());
            } else {
                print_help(argv[1]);
                return -1;
            }
            
        } else {
            std::cerr << "stegosaursrc: unknown command '" << command_name << "'" << std::endl;
            return -1;
        }
    } else {
        const char *temp = "help";
        return print_help(temp);
    }
}
